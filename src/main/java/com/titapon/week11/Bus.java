package com.titapon.week11;

public class Bus extends Vehicle implements Ambulantable {

    public Bus(String name, String engine) {
        super(name, engine);
        
    }

    @Override
    public String toString() {
        return "Bus("+this.getName()+")";
    }

    @Override
    public void ambulant() {
        System.out.println(this + " ambulant.");
        
    }


    
}
