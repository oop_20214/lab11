package com.titapon.week11;

public class Submarine extends Vehicle implements Ambulantable{

    public Submarine(String name, String engine) {
        super(name,engine);
        
    }

    @Override
    public String toString() {
        return "Submarine("+this.getName()+")";
    }


    @Override
    public void ambulant() {
        System.out.println(this + " ambulant.");
        
    }
    
}
