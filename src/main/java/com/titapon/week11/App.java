package com.titapon.week11;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       Bird bird1 = new Bird("Tom");
       bird1.eat();
       bird1.sleep();
       bird1.takeoff();
       bird1.fly();
       bird1.landing();
       bird1.walk();
       bird1.run();
       Plane boeing = new Plane("Boeing","Rosaroi");
       boeing.takeoff();
       boeing.fly();
       boeing.landing();
       Superman man1 = new Superman("Clark");
       man1.takeoff();
       man1.fly();
       man1.landing();
       man1.walk();
       man1.run();
       man1.swim();
       Bat bat1 = new Bat("Jim");
       bat1.walk();
       bat1.run();
       bat1.takeoff();
       bat1.fly();
       bat1.landing();
       bat1.sleep();
       bat1.eat();
       Rat rat1 = new Rat("Jury");
       rat1.walk();
       rat1.run();
       rat1.sleep();
       rat1.eat();
       Dog dog1 = new Dog("Fuyu");
       dog1.walk();
       dog1.run();
       dog1.sleep();
       dog1.eat();
       Cat cat1 = new Cat("Kitty");
       cat1.walk();
       cat1.run();
       cat1.sleep();
       cat1.eat();
       Snake snake1 = new Snake("Anaconda");
       snake1.swim();
       snake1.crawl();
       snake1.sleep();
       snake1.eat();
       Crocodie crocodie1 = new Crocodie("Amazon");
       crocodie1.swim();
       crocodie1.crawl();
       crocodie1.sleep();
       crocodie1.eat();
       Submarine submarine1 = new Submarine("Chalawan","Class");
       submarine1.ambulant();
       Bus bus1 = new Bus("Euro","Class");
       bus1.ambulant();
       
       Flyable[] flyables = { bird1,boeing, man1, bat1};
       for(int i = 0; i < flyables.length; i++) {
           flyables[i].takeoff();
           flyables[i].fly();
           flyables[i].landing();

       }
       Walkable[] walkables = { bird1, man1, bat1,rat1,dog1,cat1};
       for(int i = 0; i < walkables.length; i++) {
           walkables[i].walk();
           walkables[i].run();
        }

        Crawlable[] crawlables = { snake1, crocodie1};
        for(int i = 0; i < crawlables.length; i++) {
            crawlables[i].crawl();
            
        }

        Swimable[] swimables = { man1, snake1,crocodie1};
        for(int i = 0; i < swimables.length; i++) {
            swimables[i].swim();
            
        }

        Ambulantable[] ambulantables = { submarine1, bus1};
        for(int i = 0; i < ambulantables.length; i++) {
            ambulantables[i].ambulant();
            
        }




        
        
           

        




    }
}
